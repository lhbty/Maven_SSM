/**
 * LoginService.java
 * @author: lhb
 * 2017年8月7日 上午10:15:32
 */
package com.yt.Service;

import com.yt.entity.User;

/**
 * @author lhb
 *
 */
public interface LoginService {

	/**
	 * @param user
	 * @return
	 * @author: lhb
	 * 2017年8月7日 上午10:26:59
	 */
	 
	public User Login(User user);


	
	
}

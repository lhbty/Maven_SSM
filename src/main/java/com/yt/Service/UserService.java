/**
 * UserService.java
 * @author: lhb
 * 2017年6月15日 下午3:02:11
 */
package com.yt.Service;

import java.util.List;

import com.yt.entity.User;

/**
 * @author lhb
 *
 */
public interface  UserService {
	
	public User getUserById(int userId);  
	  
    public void insertUser(User user);  
  
    public void addUser(User user);  
  
    public List<User> getAllUser();

	/**
	 * @param content
	 * @author: lhb
	 * 2017年8月14日 下午1:43:19
	 */
	 
	public void saveURL(String content);

	/**
	 * @param id
	 * @return
	 * @author: lhb
	 * 2017年8月14日 下午1:48:56
	 */
	 
	public User getURL(String id);  

}

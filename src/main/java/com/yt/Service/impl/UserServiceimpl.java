/**
 * UserService.java
 * @author: lhb
 * 2017年6月15日 下午3:02:11
 */
package com.yt.Service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.yt.Dao.UserDao;
import com.yt.Service.UserService;
import com.yt.entity.User;

/**
 * @author lhb
 *
 */
@Service
public class UserServiceimpl implements UserService {
	    @Resource  
	    private UserDao userDao;  
	      
	    public User getUserById(int userId) {  
	        return userDao.queryByPrimaryKey(userId);  
	    }  
	  
	    public void insertUser(User user) {  
	    	 
	        userDao.insertUser(user);  
	    }  
	  
	    public void addUser(User user) {  
	    	System.out.println("UserImpl"+user+"========");
	        userDao.insertUser(user);  
	    }  
	    
	    @Override  
	    public List<User> getAllUser() {  
	        return userDao.getAllUser();  
	    }

		/* (non-Javadoc)
		 * @see com.yt.Service.UserService#saveURL(java.lang.String)
		 */
		@Override
		public void saveURL(String content) {
			userDao.saveURL(content);
			
		}

		/* (non-Javadoc)
		 * @see com.yt.Service.UserService#getURL(java.lang.String)
		 */
		@Override
		public User getURL(String id) {
			// TODO Auto-generated method stub
			return userDao.getURL(id);
		}  
}

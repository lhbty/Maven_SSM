/**
 * LoginServiceimpl.java
 * @author: lhb
 * 2017年8月7日 上午10:19:07
 */
package com.yt.Service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.yt.Dao.UserDao;
import com.yt.Service.LoginService;
import com.yt.entity.User;

/**
 * @author lhb
 *
 */
@Service
public class LoginServiceimpl implements LoginService {

	@Resource
	private UserDao userDao;

	
	
	
	@Override
	public User Login(User user) {
		
	return(userDao.Login(user));
		
	}



}

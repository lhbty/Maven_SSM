package com.yt.Controller;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yt.util.QRCodeUtil;

/**
 * @author jin.tang
 * @Title: springbootdemo
 * @Package com.example.demo.controller
 * @Description: ${todo}
 * @date 2017/12/25
 */
@Controller
@RequestMapping("/") 
public class QRCodeController {
	
	
	@RequestMapping("GO")
    private String GO(){
	   return "111";
}
	
    /**
     * 获得二维码
     * @param request
     * @param response
     */
    @RequestMapping(value = "phoneversion/getTwoDimension",method={RequestMethod.POST,RequestMethod.GET})
    public void getTwoDimensionForIOSs(HttpServletRequest request, HttpServletResponse response){
        try {
            QRCodeUtil.getTwoDimension("https://bbs.hupu.com/bxj", response, 300, 300);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @RequestMapping("/downloadIOSAPPQRCode")
    public ResponseEntity<byte[]> downloadIOSAPPController(/*@RequestParam(required = true)String type*/)
            throws Exception{
//        InputStream is = this.getClass().getClassLoader().getResourceAsStream("app.properties");
//        Properties props = new Properties();
//        props.load(is);
//        String appId = (String)props.get(type);
//        String url = "" + appId;
        return QRCodeUtil.getResponseEntity("http://imtt.dd.qq.com/16891/50FE065610C8A989497656F7C5B8796B.apk?fsname=com.ytjr.YinTongJinRong_1.4.0_6.apk&csr=1bbd", "C:\\Users\\YT\\Desktop\\QQ截图20180211164840.png",true,"png", 300, 300);
    }


    @RequestMapping("/showQrcode")
    public void showQrcode(HttpServletRequest request, HttpServletResponse response)
            throws Exception{
        QRCodeUtil.showQrcode("https://www.zhibo8.cc", "",response,false,"png", 300, 300);
    }


    @RequestMapping("/SaveQrCode")
    public void SaveQrCode(HttpServletRequest request, HttpServletResponse response)
            throws Exception{
        boolean flag=QRCodeUtil.SaveQrCode("https://www.zhibo8.cc", "",false,"png", 300, 300,"D:\\","qrcode");
        //log.info("flag=="+flag);
    }
}
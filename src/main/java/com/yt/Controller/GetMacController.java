/**
 * GetMacController.java
 * @author: lhb
 * 2017年11月16日 上午11:25:53
 */
package com.yt.Controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yt.Test.getmac;

/**
 * @author lhb
 *
 */
@Controller  
@RequestMapping("/getmac") 
public class GetMacController {
	
	
	@RequestMapping("/get")
	public String GetMac(){
		return "GetMac";
	}

	@RequestMapping("/getip")
	public String getip(HttpServletRequest request,Model model){
		String ip=request.getRemoteAddr();
		String mac=getmac.getMacAddress(ip);
		model.addAttribute("mac", mac);
		System.out.println(mac);
		return "GetMac";
	}
	
}

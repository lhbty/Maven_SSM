
package com.yt.Controller;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yt.util.VO;
import com.yt.util.ValidationUtil;

/**验证码接口
 * ValidationController.java
 * @author: lhb
 * 2017年9月11日 上午9:48:02
 */
@Controller
@RequestMapping("/Validation")
public class ValidationController {
	@RequestMapping("/GOValidation")
	public String GOValidation(){
		return "user/Validation";
	}
	
	/**
	 * 生成验证码
	 * @param request
	 * @param response
	 * @throws IOException
	 * @author: lhb
	 * 2017年9月11日 下午1:57:12
	 */
	@RequestMapping("image")  
	//https://localhost:8443/Maven_SSM/Validation/user/check.jpg
    public void createCode(HttpServletRequest request, HttpServletResponse response) throws IOException {  
        // 通知浏览器不要缓存  
        response.setHeader("Expires", "-1");  
        response.setHeader("Cache-Control", "no-cache");  
        response.setHeader("Pragma", "-1");  
        ValidationUtil util = ValidationUtil.Instance();  
        // 将验证码输入到session中，用来验证  
        String code = util.getString();  
        request.getSession().setAttribute("code", code);  
        // 输出打web页面  
        ImageIO.write(util.getImage(), "jpg", response.getOutputStream()); 
    }
	
	/** 
     * 验证码验证 
     * verification用户输入的验证码
     * codeSession生成保存在session中的验证码
     */  
	@RequestMapping("checkCode")  
	@ResponseBody
	private VO checkCode(HttpSession session, String verification,HttpServletRequest request) {
		try {
			// String code=verification.split(",")[1];
		        String codeSession = (String) session.getAttribute("code");  
		        if (codeSession.equalsIgnoreCase(verification)) { 
		        	request.getSession().removeAttribute("code");
		        	
		        	 System.out.println("验证成功");
		        	 return new VO(VO.SUC_CODE,"验证成功");
		        } else {  
		        	   System.out.println("验证码错误");  
		        	   return new VO(VO.FAIL_CODE,"验证码错误");
		        } 
		        
//		        if (StringUtils.isEmpty(codeSession)) {  
//		          System.err.println("没有生成验证码信息");  
//		          return new VO("没有生成验证码信息");
//		        }  
//		        if (StringUtils.isEmpty(code)) {  
//		        	   System.err.println("未填写验证码信息");  
//		        	   return new VO("未填写验证码信息");
//		        }  
//		        if (codeSession.equalsIgnoreCase(code)) {  
//		        	 System.out.println("验证成功");
//		        	 return new VO("验证成功");
//		        } else {  
//		        	   System.err.println("验证码错误");  
//		        	   return new VO("验证码错误");
//		        } 
		} catch (Exception e) {
			
			return new VO(VO.FAIL_CODE,"未知错误");
		}
       
       
    }  

}

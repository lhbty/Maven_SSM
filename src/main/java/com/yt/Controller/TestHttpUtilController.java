/**
 * TestHttpUtilController.java
 * @author: lhb
 * 2017年9月25日 上午9:47:21
 */
package com.yt.Controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yt.util.GetURL;
import com.yt.util.HttpUtil;
import com.yt.util.UploadFileTP;

/**
 * @author lhb
 *
 */
@Controller  
@RequestMapping("/TestHttpUtil")  
public class TestHttpUtilController {
	
	@RequestMapping("go")
	public String go(){
		return "TesthttpPostDownload";
	}
	
	@RequestMapping("TesthttpPostDownload")
	public String TesthttpPostDownload(Model model,HttpServletRequest request){
		String tomcatpath=GetURL.getUrl(request).get("tomcatpath").toString();
		File f2=new File(tomcatpath+"upload\\guide");
		String url=GetURL.getUrl(request).get("url").toString()+"upload/guide";
		File[] filelist=UploadFileTP.tree(f2);
		List<File> list = Arrays.asList(filelist);
		List<String> listt=new ArrayList<String>(list.size());
		for(int i=0;i<list.size();i++){
			String k=list.get(i).getName();
			k=url+"/"+k;
			listt.add(i, k);
		}
		model.addAttribute("list", listt);
		return "TesthttpPostDownload";
	}
	
	
	
	public static void main(String[] args) {
		String url="https://192.168.199.154:8443/Maven_SSM/index.jsp";
		String path="C:\\Users\\YT\\Desktop\\123\\index.jsp";
		System.out.println(123456);
		try {
			HttpUtil.httpPostDownload(url, path);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

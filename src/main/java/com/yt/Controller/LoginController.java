/**
 * LoginController.java
 * @author: lhb
 * 2017年8月7日 上午10:14:32
 */
package com.yt.Controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yt.Service.LoginService;
import com.yt.entity.User;

/**
 * @author lhb
 *
 */
@Controller  
@RequestMapping("/login")  
public class LoginController {
	
	 @Resource  
	 private LoginService loginService;
	 
	 //跳转到登陆1564456
	 @RequestMapping("/goLogin")  
	 private String goLogin(){
		return "user/Login";
	 }

	 //登陆
	  @RequestMapping("/LoginUser")  
	 private String Login(User user,HttpServletRequest request,Model model){
		User userLg=loginService.Login(user);
		request.getSession().setAttribute("user", userLg);
		model.addAttribute("user", userLg);
		if(userLg==null){
			return "error";
		}
		return "user/index";
	 }

}

/**
 * upload.java
 * @author: lhb
 * 2017年8月8日 上午9:47:34
 */
package com.yt.Controller;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.yt.util.ReadExcel;
import com.yt.util.UploadFile;
import com.yt.util.UploadFileTP;

/**
 * @author lhb
 *
 */

@Controller  
@RequestMapping("/upload")  
public class upload {
	
	
	 @RequestMapping("/go")  
	 private String goLogin(){
		return "sc";
		 
	 }
	
	@RequestMapping("/uploadingtx")
	@ResponseBody
	public String string2Image(MultipartFile file,HttpServletRequest request,MultipartFile imagesFile1,MultipartFile imagesFile2) throws IllegalStateException, IOException {  
	   try {
		   System.out.println(imagesFile1);
		   System.out.println(imagesFile2);
		   String strPath = UploadFileTP.uploadFile(file, request);//上传文件
		   List<String> listCard = ReadExcel.getIdentityCard(strPath);//读取出身份证号码
		  for(int i=0;i<listCard.size();i++){
			   String TPname= listCard.get(i);
				int number = Integer.parseInt(TPname.substring(TPname.length()-2, TPname.length()-1));//获取身份证倒数第二位
				if(number % 2 == 0){
					 UploadFile.uploadFile(imagesFile1, request, "女",TPname+".jpg");//女
				}else{
					 UploadFile.uploadFile(imagesFile2, request, "男",TPname+".jpg");//男
				}
		  }
	      return  "success"; 
	   } catch (Exception e) {
		   return  "error"; 
		}
	}     
	
	public static void main(String[] args) throws Exception {
		int j=0;
		int k=0;
		for(int i=0;i<20;i++){
			k=j++;
		}
		System.out.println(k);
		
//		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
//		System.out.println(methodName);
//		
//		upload upload=new upload();
//		upload.captureScreen("截图.png");
		
	}

	
	public void captureScreen(String fileName) throws Exception {  

		   Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();  
		   Rectangle screenRectangle = new Rectangle(screenSize);  
		   Robot robot = new Robot();  
		   BufferedImage image = robot.createScreenCapture(screenRectangle);  
		   ImageIO.write(image, "png", new File(fileName));  

		}  
}

/**判断服务器地址还是本地地址
 * GetURL.java
 * @author: lhb
 * 2017年9月8日 下午4:46:57
 */
package com.yt.util;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * @author lhb
 *tomcatpath文件路徑
 *url訪問路徑
 */
public class GetURL {
	public static  Map<String,Object> getUrl(HttpServletRequest request){
	    String IP=request.getLocalAddr();//获取ip
	    //tomcat路径
	    String tomcatpath= request.getSession().getServletContext().getRealPath("").split("webapps")[0]+"webapps\\";
	    String url="http://www.cdyintongkaifa.com:6789/";//服务器地址
	     if(IP.equals("192.168.199.154")){
	  	    url="https://192.168.199.154:8443/"; //本地地址
	     }
	     Map<String,Object> map = new HashMap<String, Object>();
	     map.put("tomcatpath", tomcatpath);
	     map.put("url", url);
		 return map;
	}
	
	public static void main(String[] args) {
		System.out.println(System.getProperty("user.dir"));
	}
}

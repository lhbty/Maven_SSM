package com.yt.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.fileupload.FileItem;

public class FileUploadUtils {

	/*------------------------获取上传文件的的文件名----------------------*/
	public static String getFileNameByCommon(FileItem fileItem) {

		return fileItem.getName().substring(fileItem.getName().lastIndexOf("\\") + 1);

	}

	/*---------------------通过UUID获取上传文件的的文件名----------------------*/
	public static String getFileNameByUUID(FileItem fileItem) {
		String filename = getFileNameByCommon(fileItem);
		String Singlefilename = UUID.randomUUID().toString() + "_" + filename;
		return Singlefilename;

	}

	/*---------------------通过时间戳获取上传文件的的文件名----------------------*/
	public static String getFileNameByTimeStamp(FileItem fileItem) {
		String filename = getFileNameByCommon(fileItem);
		String Singlefilename = System.currentTimeMillis() + "_" + filename;
		return Singlefilename;

	}

	/*--------------------- 改进之前的默认方法获取文件夹-------------------*/
	// 参数fileItem表示上传表单项 返回值为上传文件要写入的文件夹
	public static File getFileByCommon(FileItem fileItem) {
		File file = new File("c:\\img");
		if (!file.exists()) {
			file.mkdirs();
		}
		String filename = getFileNameByCommon(fileItem);
		File file2 = new File(file, filename);
		return file2;

	}

	/*----------------通过uuid实现文件名字的唯一性-------------------*/
	public static File getFileByUUIDToSingleName(FileItem fileItem) {
		File file = new File("c:\\img");
		if (!file.exists()) {
			file.mkdirs();
		}
		String Singlefilename = getFileNameByUUID(fileItem);
		File file2 = new File(file, Singlefilename);
		return file2;

	}

	/*----------------通过时间戳实现文件名字的唯一性-------------------*/
	public static File getFileByTimestampToSingleName(FileItem fileItem) {
		File file = new File("c:\\img");
		if (!file.exists()) {
			file.mkdirs();
		}
		String filename = getFileNameByCommon(fileItem);
		String Singlefilename = System.currentTimeMillis() + "_" + filename;
		File file2 = new File(file, Singlefilename);
		return file2;

	}

	/*----------------------按照日期将文件打散---------------------------*/
	public static File getFileByDataToBreakDirectory(FileItem fileItem) {
		// 获取日期值
		Long time = System.currentTimeMillis();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String data = format.format(new Date(time));
		// 创建文件夹(按照时间创建)
		File file = new File("c:\\img", data);
		if (!file.exists()) {
			file.mkdirs();
		}
		// 创建文件

		String Singlefilename = getFileNameByTimeStamp(fileItem);
		File file2 = new File(file, Singlefilename);
		return file2;

	}

	/*----------------------按照hashcode将文件打散---------------------------*/
	public static File getFileByHashCodeToBreakDirectory(FileItem fileItem) {
		/* 获取文件名称hashcode值 */
		String Singlefilename = getFileNameByTimeStamp(fileItem);
		int filenameHashcode = Singlefilename.hashCode();
		/* 将int型的hashcode的值转化成16进制的字符串 */
		String code = Integer.toHexString(filenameHashcode);
		/* 16进制有很多字符 随机取就可以 */
		String directory = code.charAt(0) + "\\" + code.charAt(1);
		// 创建2层目录
		File file = new File("c:\\img", directory);
		if (!file.exists()) {
			file.mkdirs();
		}

		File file2 = new File(file, Singlefilename);
		return file2;

	}

	/*----------------通过磁盘路径得到服务器的映射路径地址(网络访问路径)-------------------*/
	public static String getNetPath(String diskPath) {
		int index1 = diskPath.indexOf("img");
		String netPath = "http://localhost:8080/img01/"+diskPath.substring(index1 + 4).replace("\\", "/");
		return netPath;
	}
}

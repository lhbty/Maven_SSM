package com.yt.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;


public class UploadFileTP {
	
	public static String getUploadFileName(MultipartFile file) {
		String strRet = "";
		strRet = file.getOriginalFilename();
		//strRet = DateUtil.newName(strRet);
		return strRet;
	}
	
	/**
	 * 源文件上传
	 * 
	 * @param file
	 * @param folder 文件夹名称
	 * @param nId 主键
	 * @param request
	 * @return
	 * @throws IOException
	 * @throws IllegalStateException
	 */
	public static String uploadFile(MultipartFile file,HttpServletRequest request) throws IOException, IllegalStateException {
		
		String strPath = request.getSession().getServletContext().getRealPath("/")+"upload";
		File pathFile = new File(strPath);
		if (!pathFile.exists()) {
			pathFile.mkdirs(); // 创建文件夹
		}
		String strFilePath;
		strFilePath = strPath + "/" + file.getOriginalFilename();
		try {
			file.transferTo(new File(strFilePath));
			return strFilePath;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * @Description: 删除文件 
	 * @param @param strpath   
	 * @return:
	 * @throws
	 * @author zz
	 * @date 2016年11月6日 下午7:14:03
	 */
	public static void deleteFile(String strpath){
		File pathFile = new File(strpath);
		if (pathFile.exists()) {
			pathFile.delete();
			System.out.println("删除成功");
		}else{
			System.out.println("不存在");
		}
	}
	
	
	
	/**
	 * 获取文件目录集合
	 * @param file
	 * @param l
	 * @author: lhb
	 * 2017年9月25日 上午11:34:52
	 */
		public static File[] tree(File file){
			File[] childs=file.listFiles();
			for(int i=0;i<childs.length;i++){
				if(childs[i].isDirectory()){
					tree(childs[i]);
				}
			}
			return childs;
		}
		
		public static void main(String[] args) {
		  	// TODO Auto-generated method stub
				File f2=new File("D:\\apache-tomcat-7.0.56\\webapps\\upload");
				tree(f2);
			}
		

}

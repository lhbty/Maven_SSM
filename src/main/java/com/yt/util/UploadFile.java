package com.yt.util;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;


/**
 * 上传文件
 * UploadFileAction.java
 * @author zhen 
 */
@SuppressWarnings("restriction")
public class UploadFile {
	
	public static final String url="http://localhost:8080";
	//public static final String url="http://192.168.199.158:8080";
	//public static final String url="http://www.cdyintongkaifa.com:6789";
	
	
	/**
	 * 按原图大小生成新图
	 */
	public static final int CREATENEWIMAGETYPE_0 = 0;
	/**
	 * 按指定的大小生成新图
	 */
	public static final int CREATENEWIMAGETYPE_1 = 1;
	/**
	 * 按原图宽高比例生成新图-按指定的宽度
	 */
	public static final int CREATENEWIMAGETYPE_2 = 2;
	/**
	 * 按原图宽高比例生成新图-按指定的高度
	 */
	public static final int CREATENEWIMAGETYPE_3 = 3;
	/**
	 * 按原图宽高比例生成新图-按指定的宽和高中较大的尺寸
	 */
	public static final int CREATENEWIMAGETYPE_4 = 4;
	/**
	 * 按原图宽高比例生成新图-按指定的宽和高中较小的尺寸
	 */
	public static final int CREATENEWIMAGETYPE_5 = 5;
	
	/**
	 * 图片压缩
	 * 
	 * @param _file
	 *            原图片
	 * @param createType
	 *            处理类型
	 * @param newW
	 *            新宽度
	 * @param newH
	 *            新高度
	 * @return
	 * @throws Exception
	 */
	public static String createNewImage(String path) throws Exception {
		int createType = 1;
		int newW = 0;
		int newH = 0;
		// 得到原图信息
		File _file = new File(path);
		
		if (!_file.exists() || !_file.isAbsolute() || !_file.isFile())
			return null;
		Image src = ImageIO.read(_file);
		int w = src.getWidth(null);
		int h = src.getHeight(null);
		
		// 确定目标图片的大小
		int nw = w;
		int nh = h;
		if (createType == CREATENEWIMAGETYPE_0);
		else if (createType == CREATENEWIMAGETYPE_1) {
			do {
				nw = (int) (nw * 0.9);
				nh = (int) (nh * 0.9);
			} while (nw>500 || nh > 300);
		} else if (createType == CREATENEWIMAGETYPE_2) {
			nw = newW;
			nh = (int) ((double) h / (double) w * nw);
		} else if (createType == CREATENEWIMAGETYPE_3) {
			nh = newH;
			nw = (int) ((double) w / (double) h * nh);
		} else if (createType == CREATENEWIMAGETYPE_4) {
			if ((double) w / (double) h >= (double) newW / (double) newH) {
				nh = newH;
				nw = (int) ((double) w / (double) h * nh);
			} else {
				nw = newW;
				nh = (int) ((double) h / (double) w * nw);
			}
		} else if (createType == CREATENEWIMAGETYPE_5) {
			if ((double) w / (double) h <= (double) newW / (double) newH) {
				nh = newH;
				nw = (int) ((double) w / (double) h * nh);
			} else {
				nw = newW;
				nh = (int) ((double) h / (double) w * nw);
			}
		}
		// 构造目标图片
		BufferedImage tag = new BufferedImage(nw, nh, BufferedImage.TYPE_INT_RGB);

		// 得到目标图片输出流
		FileOutputStream out = new FileOutputStream(_file);

		// 根据需求画出目标图片 方式1
		tag.getGraphics().drawImage(src, 0, 0, nw, nh, null);
		
		// 将画好的目标图输出到输出流 方式1
		JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
		encoder.encode(tag);
		out.close();
		return "";
	}

	/*
	 * 文件重命名
	 */
	public static String getUploadNewFileName(String Name) {
		String strRet = Name;
		//strRet = file.getOriginalFilename();
		//strRet = DateUtil.newName(strRet);
		return strRet;
	}
	
	
	/**
	 * Description: 上传图片并制定压缩大小 
	 * @author zhen
	 * @param file  源文件
	 * @param request
	 * @param fileType 文件夹名称
	 * @return
	 * @throws IOException
	 * @throws IllegalStateException
	 */
	public static String uploadFile(MultipartFile file,	HttpServletRequest request,String fileType,String TPName)
			throws IOException, IllegalStateException {
		String strType = "upload";
		String strPath = request.getSession().getServletContext().getRealPath("/")+strType;
		//String strPath = request.getSession().getServletContext().getRealPath("/").split("Maven_SSM")[0]+strType;
		if(fileType != null ){
			strPath= strPath+"/"+fileType;
			strType="upload/"+fileType;
		}
		
		File pathFile = new File(strPath);
		if (!pathFile.exists()) {
			pathFile.mkdirs(); // 创建文件夹
		}
		String strFilePath;
		String filname = getUploadNewFileName(TPName);
		strFilePath = strPath + "/" + filname;
		try {
			file.transferTo(new File(strFilePath));
			String upname ="/" + strType + "/" + filname;
			return url+upname;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * @Description: 删除服务器图片
	 * @param @param request
	 * @param @param path	图片URL地址
	 * @param @throws IOException
	 * @param @throws IllegalStateException   
	 * @return void  
	 * @throws
	 * @author lyf
	 * @date 2016年10月26日 下午1:51:01
	 */
	public static void deleteFile(HttpServletRequest request,String path,String fileType)
			throws IOException, IllegalStateException {
		String[] arr = path.split("/");
		String strType = "upload";
		String strPath = request.getSession().getServletContext().getRealPath("/")+strType;
		//String strPath = request.getSession().getServletContext().getRealPath("/").split("Maven_SSM")[0]+strType;
		try {
			if(fileType != null ){
				strPath= strPath+"/"+fileType+"/"+arr[arr.length-1];
			}
			File file = new File(strPath);
		    if(file.isFile() && file.exists()){    
		        file.delete(); 
		        System.out.println("删除成功");
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}

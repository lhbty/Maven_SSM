package com.yt.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**  
 * @Title: ReadExcel.java
 * @Package com.cdyintong.model.util
 * @Description: 读取excel 文档
 * @author leo
 * @date 2016年11月4日 上午11:57:16
 */
public class ReadExcel {
	

	
	
	
	/**
	* @param strpath 文件路径
	* @Description:返回身份证号列表数据
	* @author:leo
	* @return 
	* @date 2016年12月16日下午11:28:13
	* @throws
	*/
	public static List<String> getIdentityCard(String strpath){
		List<String> list = new ArrayList<String>();
		Workbook  hssfWorkbook = null;
	    InputStream is = null;
	    int index = 0;
		try {
			is = new FileInputStream(strpath);
			hssfWorkbook  = new XSSFWorkbook(is);
			for(int i=0;i<hssfWorkbook.getNumberOfSheets();i++){ //页脚总数
				XSSFSheet sheet = (XSSFSheet) hssfWorkbook.getSheetAt(i);
				XSSFRow row0 = sheet.getRow(0); //获取第一行的值
				
				/**
				 * 获取身份证号的数据列位置
				 */
				if(row0 != null){
					for (int j = 0; j < row0.getLastCellNum(); j++) {
						String name = row0.getCell(j).getStringCellValue(); //取得某单元格内容，字符串类型
						if(name.equals("身份证号") || name.equals("身份证号码") || name.equals("身份证") || name.equals("居民身份证号码")){
							index = j;
						}
					}
					/**
					 * 读取数据
					 */
					for(int rows=1; rows <= sheet.getLastRowNum();rows++){//有多少行
						XSSFRow row = sheet.getRow(rows);//取得某一行   对象
						if (row != null) {
							//为空跳出循环
							if(row.getCell(index).equals("")||row.getCell(index).getStringCellValue().equals(""))continue;
							list.add(row.getCell(index) == null?"1":row.getCell(index).getStringCellValue());
						}
					}
				}
				
			}
		} catch (Exception e) {
			index = 0;
			if(is != null){
				try {
					is.close();
					is = new FileInputStream(strpath);
					hssfWorkbook = new HSSFWorkbook(is);
					 for (int numSheet = 0; numSheet < hssfWorkbook.getNumberOfSheets(); numSheet++) {
			        	 HSSFSheet hssfSheet = (HSSFSheet) hssfWorkbook.getSheetAt(numSheet);
			        	 HSSFRow hssfRow1 = hssfSheet.getRow(numSheet); //获取第一行数据
			        	 //获取数据列
			        	 if(hssfRow1 != null){
			        		 for (int i = 1; i <= hssfRow1.getLastCellNum(); i++) {
			        			 if(hssfRow1 != null && hssfRow1.getCell(i)!= null){
				        			 String name = hssfRow1.getCell(i).getStringCellValue();
				        			 if(name.equals("身份证号") || name.equals("身份证号码") || name.equals("身份证") || name.equals("居民身份证号码")){
				        				 index = i;
				        			 }
				        		 }
							 }
			        		 /**
							 * 循环行Row 读取数据 
							 */
				        	 for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
				        		 HSSFRow hssfRow = hssfSheet.getRow(rowNum);
				        		 if (hssfRow != null) {
				        			 list.add(hssfRow.getCell(index)==null?"1":hssfRow.getCell(index).getStringCellValue());
				        		 }
				        	 }
			        	 }
					 }
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
		return list;
	}
	
	
	/**
	 * @Description: 判断字符串是否为数字 
	 * @param @param str
	 * @param @return   
	 * @return:
	 * @throws
	 * @author zz
	 * @date 2016年11月6日 上午11:30:27
	 */
	public static boolean isNumeric(String str){
	  for (int i = 0; i < str.length(); i++){
		   if (!Character.isDigit(str.charAt(i))){
		    return false;
		   }
	  }
	  return true;
	 }
	
	/**
	 * @Description: 
	 * @param @param name 名称
	 * @param @param columnIndex  下标
	 * @param @param map   
	 * @return:
	 * @throws
	 * @author zz
	 * @date 2016年11月6日 下午6:01:08
	 */
	public static void putMap(String name,int columnIndex,Map<String,Integer> map){
		if(name.equals("姓名")){
			 map.put("name", columnIndex);
		 }else if(name.equals("性别")){
			 map.put("sex", columnIndex);
		 }else if(name.equals("民族")){
			 map.put("nation", columnIndex);
		 }else if(name.equals("联系电话")){
			 map.put("phone", columnIndex);
		 }else if(name.equals("身份证号") || name.equals("身份证号码")){
			 map.put("identityCard", columnIndex);
		 }else if(name.equals("现住址")){
			 map.put("address", columnIndex);
		 }else if(name.equals("所属机构")){
			 map.put("organization",columnIndex);
		 }else if(name.equals("新农合证（卡）号") || name.equals("新农合证(卡)号")){
			 map.put("cooperative",columnIndex);
		 }else if(name.equals("健康档案号") || name.endsWith("健康档案编号")){
			 map.put("recordNum",columnIndex);
		 }else if(name.equals("县")){
			 map.put("country",columnIndex);
		 }else if(name.equals("镇")){
			 map.put("town",columnIndex);
		 }else if(name.equals("村")){
			 map.put("village",columnIndex);
		 }else if(name.equals("组")){
			 map.put("groupe",columnIndex);
		 }else if(name.equals("银行卡号") || name.equals("银行卡") || name.equals("银行账号")){
			 map.put("bankCard", columnIndex);
		 }
	}
	
	
	public static String boleanValid(XSSFCell xssfCell){
		if(xssfCell == null){
			return null;
		}else{
			xssfCell.setCellType(xssfCell.CELL_TYPE_STRING);//将数据类型设置为String
			return xssfCell.getStringCellValue();
		}
	}
	
	public static String boleanValid(HSSFCell xssfCell){
		if(xssfCell == null){
			return null;
		}else{
			xssfCell.setCellType(xssfCell.CELL_TYPE_STRING);//将数据类型设置为String
			return xssfCell.getStringCellValue();
		}
	}
	
	


	
	public ReadExcel() {
		super();
	}
	
	
	
	
}

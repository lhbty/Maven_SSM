﻿package com.yt.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @Title: DateUtil.java
 * @Description: 日期辅助类
 * @version V1.0
 */
public class DateUtil {

	/**
	 * 返回当前时间　格式：yyyy-MM-dd HH:mm:ss
	 * 
	 * @return String
	 */
	public static String fromDateNonSpaceH() {
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
		return format1.format(new Date());
	}

	/**
	 * 年月日时分秒毫秒(无下划线) yyyyMMddHHmmssSSS
	 */
	private static final String dtLong = "yyyyMMddHHmmssSSS";
	
	/**
	 * 返回当前时间　格式：yyyy-MM-dd HH:mm:ss
	 * 
	 * @return String
	 */
	public static String fromDate() {
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format1.format(new Date());
	}

	/**
	 * 返回当前时间　格式：yyyy-MM-dd HH-mm-ss
	 * 
	 * @return String
	 */
	public static String fromDateFile() {
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SS");
		return format1.format(new Date());
	}

	/**
	 * 返回当前时间　格式：yyyy-MM-dd HH:mm:ss
	 * 
	 * @return String
	 */
	public static String fromDateHNonSpace() {
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
		return format1.format(new Date());
	}

	/**
	 * 返回当前时间　格式：yyyy-MM-dd
	 * 
	 * @return String 格式：yyyy-MM-dd
	 */
	public static String fromDateY() {
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		return format1.format(new Date());
	}
	
	/**
	 * @Title: getDateString
	 * @Description: 使用"yyyy-MM-dd HH:mm"格式化日期
	 * @param date
	 * @return String 返回类型
	 */
	public static String getDateStringH(Date date) {
		return getDateString(date, "yyyy-MM-dd HH:mm:ss");
	}
	
	/**
	 * @Title: getDateString
	 * @Description: 使用"yyyy-MM-dd"格式化日期
	 * @param date
	 * @return yyyy-MM-dd
	 */
	public static String getDateStringY(Date date) {
		return getDateString(date, "yyyy-MM-dd");
	}
	
	/**
	 * @Title: getDateString
	 * @Description: 格式化日期
	 * @param date
	 *            日期
	 * @param format
	 *            模式
	 * @return String
	 */
	public static String getDateString(Date date, String format) {
		if (date != null) {
			SimpleDateFormat formatter = new SimpleDateFormat(format);
			String dateString = formatter.format(date);
			return dateString;
		}
		return null;
	}

	/**
	 * 
	 * 功能描述：String 转 timestamp
	 * 
	 * @param time
	 * @return
	 * 
	 * @author 刘真
	 * 
	 * 
	 * @update:[变更日期YYYY-MM-DD][更改人姓名][变更描述]
	 */
	public static Timestamp getTimestamp(String time) {
		return Timestamp.valueOf(time);
	}

	/**
	 * 
	 * 功能描述：date 转 timestamp
	 * 
	 * @param date
	 * @return
	 * 
	 * @author 刘真
	 * 
	 * 
	 * @update:[变更日期YYYY-MM-DD][更改人姓名][变更描述]
	 */
	public static Timestamp getTimestamp(Date date) {
		return new Timestamp(date.getTime());
	}

	/**
	 * 根据日期字符串解析为Date对象,解析格式yyyy-MM-dd
	 * 
	 * @param dateStr
	 *            日期字符串
	 * @return
	 */
	public static Date fromDateY(String dateStr) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return format.parse(dateStr);
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * Description: 返回当前日期
	 * 
	 * @return yyyy-MM-dd
	 * @author:zhen 2016年1月9日下午4:03:23
	 */
	public static Date getDate() {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String date = format.format(new Date());
		try {
			return format.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return new Date();
		}
	}

	/**
	 * 根据日期字符串解析为Date对象,解析格式yyyy-MM-dd HH:mm:ss
	 * 
	 * @param dateStr
	 *            日期字符串
	 * @return
	 */
	public static Date fromDateH(String dateStr) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {
			return format.parse(dateStr);
		} catch (ParseException e) {
			format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				return format.parse(dateStr);
			} catch (ParseException e1) {
				return null;
			}
		}
	}

	/**
	 * 计算日期返回格式化后的字符串
	 * 
	 * @param date
	 *            需要计算日期对象
	 * @param field
	 *            需要计算的部分（参照Calendar.field）
	 * @param value
	 *            计算的值
	 * @return 返回计算后的日期字符串的值（格式：yyyy-MM-dd）
	 * 
	 * @author 刘真
	 */
	public static String calcDateY(Date date, int field, int value) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(field, value);
		return getDateStringY(cal.getTime());
	}

	/**
	 * 计算日期返回格式化后的字符串
	 * 
	 * @param date
	 *            需要计算日期对象
	 * @param field
	 *            需要计算的部分（参照Calendar.field）
	 * @param value
	 *            计算的值
	 * @return 返回计算后的日期字符串的值（格式：yyyy-MM-dd HH:mm:ss）
	 * 
	 * @author 刘真
	 */
	public static String calcDateH(Date date, int field, int value) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(field, value);
		return getDateStringH(cal.getTime());
	}

	/**
	 * 计算日期返回日期对象
	 * 
	 * @param date
	 *            需要计算日期对象
	 * @param field
	 *            需要计算的部分（参照Calendar.field）
	 * @param value
	 *            计算的值
	 * @return 返回计算后的日期对象
	 * 
	 * @author 刘真
	 */
	public static Date calcDate(Date date, int field, int value) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(field, value);
		return cal.getTime();
	}

	/**
	 * 返回系统当前时间(精确到毫秒),作为一个唯一的订单编号
	 * 
	 * @return 以yyyyMMddHHmmss为格式的当前系统时间
	 */
	public static String getOrderNum() {
		Date date = new Date();
		DateFormat df = new SimpleDateFormat(dtLong);
		return df.format(date);
	}

	public static int daysBetwwen(Date startDate, Date endDate) {
		long time1 = startDate.getTime();
		long time2 = endDate.getTime();
		return Integer.parseInt(String.valueOf((time2 - time1)
				/ (1000 * 3600 * 24)));
	}

	/**
	 * 文件重命名
	 * 
	 * @param originalName
	 * @return
	 */
	public static String newName(String originalName) {
		String strTime, strName, strSuffix;
		int nTemp = originalName.lastIndexOf('.');

		strName = originalName.substring(0, nTemp);
		if (strName.length() > 230) {
			// 保证图片文件名不超过255个字符
			strName = strName.substring(0, 229);
		}
		strSuffix = originalName.substring(nTemp, originalName.length());
		strTime = DateUtil.fromDateFile();

		return strTime + strSuffix;
	}

	/**
	 * Description: 指定日期增加天数计算日期
	 * 
	 * @param calDate
	 * @param addDate
	 * @return yyyy-MM-dd
	 * @author:zhen 2016年1月10日下午10:04:23
	 * @throws ParseException
	 */
	public static Date addCalendarDay(Date calDate, int addDate)
			throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cl = Calendar.getInstance();
		cl.setTime(calDate);
		cl.add(Calendar.DATE, addDate);
		String temp = "";
		temp = dateFormat.format(cl.getTime());
		return dateFormat.parse(temp);
	}

}
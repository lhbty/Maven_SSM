package com.yt.util;

/**  
 * @Title: VO.java
 * @Package com.cdyintong.model.util
 * @Description: App 返回json 封装类
 * @author leo
 * @date 2016年11月14日 下午4:36:46
 */
public class VO {
	
	private int code;
	private String msg;
	private Object obj;	//返回的数据
	
	public static int FAIL_CODE = 2;
	public static int SUC_CODE = 1;
	
	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(int code) {
		this.code = code;
	}
	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}
	
	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
	/**
	* <p>Title: </p>
	* <p>Description: </p>
	* @param code
	* @param msg
	*/ 
	public VO(int code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}
	
	/**
	 * @return the obj
	 */
	public Object getObj() {
		return obj;
	}
	/**
	 * @param obj the obj to set
	 */
	public void setObj(Object obj) {
		this.obj = obj;
	}
	/**
	* <p>Title: </p>
	* <p>Description: </p>
	*/ 
	public VO() {
		this.code = SUC_CODE;
		this.msg = "成功";
	}
	
	public VO(int code){
		this.code = FAIL_CODE;
		this.msg = "失败";
	}
	
	/**
	* <p>Title: </p>
	* <p>Description: </p>
	* @param code
	* @param msg
	* @param obj
	*/ 
	public VO(int code, String msg, Object obj) {
		super();
		this.code = code;
		this.msg = msg;
		this.obj = obj;
	}
	
	/**
	* <p>Title: </p>
	* <p>Description: </p>
	* @param obj
	*/ 
	public VO(Object obj) {
		super();
		this.obj = obj;
		this.code = SUC_CODE;
		this.msg = "成功";
	}
	
	public VO(int code,Object obj){
		super();
		this.obj = obj;
		this.code = code;
		this.msg = "异常";
	}
	
	
	
}

package com.yt.util;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Demo {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String str=sc.nextLine();
        String path="D:\\Test\\123.txt";
        System.out.println(howMany(path,str));
        System.out.println(appearTimes(path,str));
        //
        System.out.println(appearTimes1(path,str));
    }
    public static int howMany(String path,String str){
        File file=new File(path);
        int count = 0;
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String strFile;
            while((strFile=in.readLine())!=null) {
                System.out.println(strFile);
                for (int i = 0; i <= strFile.length() - str.length(); i++) {
                    if (strFile.substring(i).startsWith(str))
                        count++;
                }
            }
        }catch(IOException exc){
            exc.printStackTrace();
        }
        return count;
    }
    public static int appearTimes(String path,String str){
        File file=new File(path);
        int count = 0;
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String strFile;
            while((strFile=in.readLine())!=null) {
                //System.out.println(strFile);
                Pattern p=Pattern.compile(str);
                Matcher m=p.matcher(strFile);
                while(m.find())
                    count++;
            }
        }catch(IOException exc){
            exc.printStackTrace();
        }
        return count;
    }
    public static int appearTimes1(String path,String str){
        File file=new File(path);
        int count = 0;
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String strFile;
            while((strFile=in.readLine())!=null) {
                //System.out.println(strFile);
                int index = 0;
                while ((index = strFile.indexOf(str, index)) != -1) {
                    index++;
                    count++;
                }
            }
        }catch(IOException exc){
            exc.printStackTrace();
        }
        return count;
    }
}



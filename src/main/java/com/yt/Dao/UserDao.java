package com.yt.Dao;  
  
import java.util.List;
import java.util.Map;

import com.yt.entity.User;
  
public interface UserDao {  
      
    public User queryByPrimaryKey(Integer id);  
      
    public List<User> queryUserByBatch(Map<String,Object> params);  
      
    public void insertUser(User user);  
      
    public void insertUserByBatch(List<User> list);  
      
    public void deleteByPrimaryKey(Integer id);  
      
    public void delteUserByBatch(Map<String,Object> params);  
      
    public void updateByPrimaryKey(Integer id);  
  
    public List<User> getAllUser();  
      
	public User Login(User user);

	/**
	 * @param content
	 * @author: lhb
	 * 2017年8月14日 下午1:43:45
	 */
	 
	public void saveURL(String content);

	/**
	 * @param id
	 * @return
	 * @author: lhb
	 * 2017年8月14日 下午1:49:25
	 */
	 
	public User getURL(String id);

	/**
	 * @param content
	 * @author: lhb
	 * 2017年8月14日 上午10:47:01
	 */

}  
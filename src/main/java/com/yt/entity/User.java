/**
 * User.java
 * @author: lhb
 * 2017年6月15日 下午3:04:07
 */
package com.yt.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author lhb
 *
 */
@JsonInclude(Include.NON_NULL)
public class User {  
    /* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	

	private Integer id;  
  
    /* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", password=" + password
				+ ", age=" + age + ", content=" + content + "]";
	}

	private String name;  
  
    private String password;  
  
    private Integer age;  
    
    private String content;
  
    /**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	public Integer getId() {  
        return id;  
    }  
  
    public void setId(Integer id) {  
        this.id = id;  
    }  
  
    public String getName() {  
        return name;  
    }  
  
    public void setName(String name) {  
        this.name = name;  
    }  
  
    public String getPassword() {  
        return password;  
    }  
  
    public void setPassword(String password) {  
        this.password = password == null ? null : password.trim();  
    }  
  
    public Integer getAge() {  
        return age;  
    }  
  
    public void setAge(Integer age) {  
        this.age = age;  
    }  
}  

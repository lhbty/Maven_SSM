/**
 * ReferencePkValue2.java
 * @author: lhb
 * 2017年7月6日 下午2:00:59
 */
package com.yt.Test;

import java.io.IOException;
import java.text.ParseException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * @author lhb
 *
 */
public class ReferencePkValue2 {
	  public static void main(String[] args) { 
		  //解析Url获取Document对象
  String html = "<html><head><title>First parse</title></head>"
			  + "<body><p>Parsed HTML into a doc.</p></body></html>";
			Document document = Jsoup.parse(html);
			
      //     Document document =  Jsoup.connect("").get();
		//获取网页源码文本内容
		System.out.println(document.toString());
		//获取指定class的内容指定tag的元素
		Elements liElements = document.getElementsByTag("title");
		for (int i = 0; i < liElements.size(); i++) {
		    System.out.println(i + ". " + liElements.get(i).text());
		}
	    
		}
}

		

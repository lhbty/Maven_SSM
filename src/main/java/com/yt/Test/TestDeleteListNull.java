/**
 * TestDeleteListNull.java
 * @author: lhb
 * 2017年10月16日 上午11:46:02
 */
package com.yt.Test;

import java.util.ArrayList;
import java.util.List;

import com.yt.entity.User;

/**
 * @author lhb
 *
 */
public class TestDeleteListNull {
	
	public static void main(String[] args) {
		 User user1=new User();
		 User user2=new User();
		 user1.setName("1");
		 user1.setAge(11);
		 user1.setId(1);
		 
		 user2.setName("2");
		 user2.setAge(22);
		 user2.setPassword("222");
		 ArrayList<User> list = new ArrayList<User>();
		 list.add(user1);
		 list.add(user2);
		 
		 List<String> list2 = new ArrayList<String>();
		 
	        list2.add(null);
	        list.removeAll(list2);
		 System.out.println(list);

	 }


}

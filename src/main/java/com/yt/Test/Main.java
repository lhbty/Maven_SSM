package com.yt.Test;
import java.util.Scanner;

public class Main{
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        while(sc.hasNext()){
            int n=sc.nextInt();
            int[][] arr=new int[n][n];
            int cur=1;
            int i=0;
            int j=n/2;
            arr[i][j]=cur;
            while(++cur<=n*n){
                if((cur-1)%n!=0){
                    i=(i-1+n)%n;
                    j=(j+1+n)%n;
                    arr[i][j]=cur;
                }else{
                    i=(i+1+n)%n;
                    arr[i][j]=cur;
                }
            }

            for(i=0;i<n;i++){
                for(j=0;j<n;j++){
                    System.out.print(arr[i][j]+"\t");
                }
                System.out.println();
            }
        }
    }
}
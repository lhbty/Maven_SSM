package com.yt.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import redis.clients.jedis.BinaryClient.LIST_POSITION;
import redis.clients.jedis.Jedis;

/**
 * @author dotleo
 *
 */
public class TestRedis {
	
	//Java中操作Redis的对象
	private Jedis jedis ;
	
	@Before
	public void connection() {
		
		//连接Redis服务器，参数1为ip，参数2为端口号，请根据自己实际情况赋值
		jedis = new Jedis("192.168.199.154",6379);
	}
	
	/**
	 * redis字符串操作
	 */
	@Test
	public void stringTest() {
		
		//为避免多次运行Redis中存在键，导致结果不符合预期，每次执行时清理当前库
		//特别注意：在开发中请勿如此操作
		jedis.flushDB();
		
		//添加字符串
		jedis.set("age", "101");
		System.out.println("age = " + jedis.get("age"));
		//批量添加字符串
		jedis.mset("age1","1","age2","2");
		System.out.println("age1 = " + jedis.get("age1"));
		System.out.println("age2 = " + jedis.get("age2"));
		//添加字符串（仅在不存在时）
		jedis.setnx("price", "101");
		System.out.println("price = " + jedis.get("price"));
		//加1操作
		jedis.incr("price");
		System.out.println("price = " + jedis.get("price"));
		//拼接字符串
		jedis.append("age", "years");
		System.out.println("age = " + jedis.get("age"));
		//截取字符串
		String str = jedis.getrange("age", 0L, 5L);
		System.out.println("age[0,5] = " + str);
		//获取字符串长度
		Long len = jedis.strlen("age");
		System.out.println("age length = " + len);
		//删除字符串
		jedis.del("age");
		System.out.print("age = " + jedis.get("age"));
	}
	
	/**
	 * redis哈希操作
	 */
	@Test
	public void hashTest() {
		
		//为避免多次运行Redis中存在键，导致结果不符合预期，每次执行时清理当前库
		//特别注意：在开发中请勿如此操作
		jedis.flushDB();
		
		//添加值
		jedis.hset("student", "name", "zhangsan");
		//添加值（仅在不存在时）
		jedis.hsetnx("student", "age", "12");
		//批量添加值
		Map<String,String> map = new HashMap<String,String>();
		map.put("sex", "boy");
		map.put("address", "beijing");
		jedis.hmset("student", map);
		//获取值
		String str = jedis.hget("student", "name");
		System.out.println("student:name = " + str);
		//批量获取值
		List<String> list = jedis.hmget("student", "name","age");
		System.out.println("student:name = " + list.get(0));
		System.out.println("student:age = " + list.get(1));
		//获取key
		Set<String> set = jedis.hkeys("student");
		System.out.println("student keys:");
		for (String string : set) {
			System.out.println(string);
		}
		//获取值
		list = jedis.hvals("student");
		System.out.println("student vals:");
		for (String string : list) {
			System.out.println(string);
		}
		//获取key和值
		map = jedis.hgetAll("student");
		System.out.println("student keys vals:");
		for (String key:map.keySet()) {
			System.out.println(key + " = " + map.get(key));
		}
		//删除值
		jedis.hdel("student", "sex");
		System.out.println("student:sex = " + jedis.hget("student", "sex"));
		//获取长度
		Long len = jedis.hlen("student");
		System.out.println("student length = " + len);
	}
	
	/**
	 * redis列表操作
	 */
	@Test
	public void listTest() {
		
		//为避免多次运行Redis中存在键，导致结果不符合预期，每次执行时清理当前库
		//特别注意：在开发中请勿如此操作
		jedis.flushDB();
		
		//表头添加
		jedis.lpush("student", "xiaoli","xiaowang","xiaoliu","xiaozhang");
		//表尾添加
		jedis.rpush("student", "zhangsan","lisi","wangwu");
		//通过索引获取元素
		String str = jedis.lindex("student", 1L);
		System.out.println("student 1st = " + str);
		//在元素前或者后插入元素
		jedis.linsert("student", LIST_POSITION.BEFORE, "xiaozhang", "zhangsan");
		//从表头弹出
		str = jedis.lpop("student");
		System.out.println("student first = " + str);
		//从表尾弹出
		str = jedis.rpop("student");
		System.out.println("student end = " + str);
		//删除
		jedis.lrem("student", 2, "zhangsan");
		//获取所有值
		List<String> list = jedis.lrange("student", 0L, -1L);
		System.out.println("student vals:");
		for (String string : list) {
			System.out.println(string);
		}
	}
	
	/**
	 * redis集合操作
	 */
	@Test
	public void setTest() {

		//为避免多次运行Redis中存在键，导致结果不符合预期，每次执行时清理当前库
		//特别注意：在开发中请勿如此操作
		jedis.flushDB();
		
		//添加成员
		jedis.sadd("student", "zhangsan","lisi");
		jedis.sadd("monitor","wangwu","zhangsan");
		//获取成员数
		Long count = jedis.scard("student");
		System.out.println("student count = " + count);
		//获取成员
		Set<String> set = jedis.smembers("student");
		System.out.println("student members:");
		for (String string : set) {
			System.out.println(string);
		}
		//是否为该集合的成员
		boolean bool = jedis.sismember("student", "zhangsan");
		System.out.println("member zhangsan exist?\n" + bool);
		//交集
		set = jedis.sinter("student","monitor");
		System.out.println("student inter monitor members:");
		for (String string : set) {
			System.out.println(string);
		}
		//并集
		set = jedis.sunion("student","monitor");
		System.out.println("student union monitor members:");
		for (String string : set) {
			System.out.println(string);
		}
		//补集
		set = jedis.sdiff("student","monitor");
		System.out.println("student diff monitor members:");
		for (String string : set) {
			System.out.println(string);
		}
		//随机返回成员
		String str = jedis.srandmember("student");
		System.out.println("random member:\n" + str);
		//移动成员
		jedis.smove("student", "monitor", "lisi");
		//删除成员
		jedis.srem("monitor", "zhangsan");
		//显示
		set = jedis.smembers("student");
		System.out.println("student members:");
		for (String string : set) {
			System.out.println(string);
		}
		set = jedis.smembers("monitor");
		System.out.println("monitor members:");
		for (String string : set) {
			System.out.println(string);
		}
	}
	
	/**
	 * redis有序集合操作
	 */
	@Test
	public void sortSetTest() {

		//为避免多次运行Redis中存在键，导致结果不符合预期，每次执行时清理当前库
		//特别注意：在开发中请勿如此操作
		jedis.flushDB();
		
		//添加成员
		Map<Double,String> map = new HashMap<Double,String>();
		map.put(50d, "zhangsan");
		map.put(60d, "lisi");
		map.put(70d, "wangwu");
		map.put(80d, "zhaoliu");
		map.put(90d, "yangqi");
		map.put(120d, "xiaoming");
		map.put(130d, "xiaozhang");
		map.put(140d, "xiaoli");
		map.put(150d, "xiaoliu");
	//	jedis.zadd("score", map);
		//更新分数
		jedis.zadd("score", 100d, "zhangsan");
		//获取成员的分数值
		Double dou = jedis.zscore("score", "zhangsan");
		System.out.println("zhangsan score :\n" + dou);
		//按照成员分数小到大返回索引区间的成员
		Set<String> set = jedis.zrange("score", 0, -1);
		System.out.println("score member order:");
		for (String string : set) {
			System.out.println(string);
		}
		//按照成员分数大到小返回索引区间的成员
		set = jedis.zrevrange("score", 0, -1);
		System.out.println("score member reverse:");
		for (String string : set) {
			System.out.println(string);
		}
		//按照成员分数小到大返回分数值区间的成员
		set = jedis.zrangeByScore("score", 50d, 80d);
		System.out.println("score member order:");
		for (String string : set) {
			System.out.println(string);
		}
		//返回有序集合中指定成员的索引
		Long index = jedis.zrank("score", "xiaoliu");
		System.out.println("xiaoliu index is:\n" + index);
	}
}

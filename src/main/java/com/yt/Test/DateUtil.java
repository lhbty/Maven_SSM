package com.yt.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 日期操作公共类
 * @author wangxin
 * @version dayuanV1.0
 * 
 */


public class DateUtil {

	private static Logger logger = LoggerFactory.getLogger(DateUtil.class);
	
	/** yyyy-MM-dd. */
	public static final String FORMAT_YMD_1 = "yyyy-MM-dd";

	/** yyyy/MM/dd. */
	public static final String FORMAT_YMD_2 = "yyyy/MM/dd";

	/** yyyyMMdd. */
	public static final String FORMAT_YMD_3 = "yyyyMMdd";

	/** yy/MM/dd. */
	public static final String FORMAT_YMD_4 = "yy/MM/dd";

	/** yyyy/M/d. */
	public static final String FORMAT_YMD_5 = "yyyy/M/d";

	/** yyMMdd. */
	public static final String FORMAT_YMD_6 = "yyMMdd";

	/** yyyy.MM.dd. */
	public static final String FORMAT_YMD_7 = "yyyy.MM.dd";

	/** yyyyMM. */
	public static final String FORMAT_YM_1 = "yyyyMM";

	/** yyyy/MM. */
	public static final String FORMAT_YM_2 = "yyyy/MM";
	/** yyyy-MM. */
	public static final String FORMAT_YM_3 = "yyyy-MM";

	/** yyyy-MM-dd HH:mm:ss. */
	public static final String FORMAT_DATE_TIME_1 = "yyyy-MM-dd HH:mm:ss";

	/** yyyy/MM/dd HH:mm:ss. */
	public static final String FORMAT_DATE_TIME_2 = "yyyy/MM/dd HH:mm:ss";

	/** yyyyMMddHHmmss. */
	public static final String FORMAT_DATE_TIME_3 = "yyyyMMddHHmmss";

	/** yyyy-MM-dd HH:mm. */
	public static final String FORMAT_DATE_TIME_4 = "yyyy-MM-dd HH:mm";

	/** yyyy/MM/dd HH:mm. */
	public static final String FORMAT_DATE_TIME_5 = "yyyy/MM/dd HH:mm";

	/** yyyy-MM-dd HH:mm:ss.SSS. */
	public static final String FORMAT_DATE_TIME_7 = "yyyy-MM-dd HH:mm:ss.SSS";
	
	/** MM月dd日*/ //added by wangxin at 2017-10-13
	public static final String FORMAT_DATE_TIME_8 = "MM月dd日";

	/** yyyy. */
	public static final String FORMAT_YEAR_1 = "yyyy";

	/** MM. */
	public static final String FORMAT_MONTH_1 = "MM";

	/** HH:mm:ss. */
	public static final String FORMAT_TIME_1 = "HH:mm:ss";
	
	/** HH:mm:ss. */
    public static final String FORMAT_TIME_2 = "HHmmss";

	/** HH:mm. */
	public static final String FORMAT_HM_1 = "HH:mm";

	/** HHmm. */
	public static final String FORMAT_HM_2 = "HHmm";

	/** dd. */
	public static final String FORMAT_DATE_1 = "dd";

	/** HH. */
	public static final String FORMAT_HOUR_1 = "HH";

	/**
	 * 构造函数
	 * 
	 */
	private DateUtil() {

	}

	/**
	 * 获取输入年月的最后一天
	 * 
	 * @param date
	 * @return
	 */
	public static int getLastDayOfMonth(Date date) {

		Calendar calender = Calendar.getInstance();
		calender.setTime(date);

		return calender.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 将一个字符日期格式化
	 * 
	 * @param strDate
	 * @param formatFrom
	 * @param formatTo
	 * @return
	 * @throws ParseException
	 */
	public static String formatDate(String strDate, String formatFrom,
			String formatTo) throws ParseException {

		if (isEmpty(strDate)) {
			return strDate;
		}

		SimpleDateFormat ff = new SimpleDateFormat(formatFrom);
		ff.setLenient(false);

		SimpleDateFormat ft = new SimpleDateFormat(formatTo);
		ft.setLenient(false);

		return ft.format(ff.parse(strDate));

	}
	
		/**
		 * 将一个字符格式的日期转换为Date
	 * 
	 * @param date
	 * @param strFormat
	 * @return
	 */
	public static Date toDate(String date, String strFormat) {
		if (isEmpty(date)) {
			return null;
		}

		try {
			SimpleDateFormat df = new SimpleDateFormat(strFormat);
			df.setLenient(false);
			Date objDate = df.parse(date);
			return objDate;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 判断一个字符是否是日期格式
	 * 
	 * @param date
	 * @param strFormat
	 * @return
	 */

	public static boolean isData(String date, String strFormat) {
		if (isEmpty(date)) {
			return true;
		}

		try {
			SimpleDateFormat df = new SimpleDateFormat(strFormat);
			df.setLenient(false);
			df.parse(date);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * 将一个字符格式的日期转换为SQLDate
	 * 
	 * @param date
	 * @param strFormat
	 * @return
	 */
	public static java.sql.Date toSqlDate(String date, String strFormat) {
		if (isEmpty(date)) {
			return null;
		}

		try {
			SimpleDateFormat df = new SimpleDateFormat(strFormat);
			df.setLenient(false);
			Date objDate = df.parse(date);
			return new java.sql.Date(objDate.getTime());
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 将一个Date格式化为字符
	 * 
	 * @param date
	 * @param toFormat
	 * @return
	 */
	public static String toString(Date date, String toFormat) {

		if (date == null) {
			return null;
		}

		SimpleDateFormat df = new SimpleDateFormat(toFormat);
		df.setLenient(false);
		return df.format(date);
	}

	/**
	 * 获取昨天时间
	 * 
	 * @return
	 */
	public static Date getYesterday() {
		Calendar calender = Calendar.getInstance();
		calender.setTime(new Date());
		calender.add(Calendar.DAY_OF_MONTH, -1);
		return calender.getTime();

	}

	/**
	 * 获取当前时间的上一个小时时间
	 * 
	 * @return
	 */
	public static Date getLastOnHourTime(Date date) {
		Calendar calender = Calendar.getInstance();
		calender.setTime(date);
		calender.add(Calendar.HOUR_OF_DAY, -1);
		return calender.getTime();

	}

	/**
	 * 获取上月日期
	 * 
	 * @return
	 */
	public static Date getLastMonth() {
		Calendar calender = Calendar.getInstance();
		calender.setTime(new Date());
		calender.add(Calendar.MONTH, -1);
		return calender.getTime();
	}

	/**
	 * 获取一个指定日的时间
	 * 
	 * @return
	 */
	public static Date getDateByDay(int day) {
		Calendar calender = Calendar.getInstance();
		calender.setTime(new Date());
		calender.set(Calendar.DAY_OF_MONTH, day);
		return calender.getTime();
	}

	/**
	 * 返回开始日期到结束日期（包括二者）之间的天数
	 * @author wangxin
	 * @param beginDate
	 *            开始日期
	 * @param endDate
	 *            结束日期
	 * @return long 天数
	 */
	@SuppressWarnings("deprecation")
	public static long getDays(String beginDate, String endDate) {
		long days= -1;
		if(!isOneMonth(beginDate,endDate)){
			try {
				Date d1 = new Date(beginDate.replace("-", "/"));
				Date d2 = new Date(endDate.replace("-", "/"));
				days= (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24);
			} catch (Exception e) {
				days= -1;
			}
		}
		return days;
	}
	
	/**
	 * 返回开始日期到结束日期（包括二者）之间的天数
	 * @author wangxin
	 * @param beginDate
	 *            开始日期
	 * @param endDate
	 *            结束日期
	 * @return long 天数
	 */
	private static boolean isOneMonth(String beginDate, String endDate) {
		boolean b = false;
		if(beginDate!=null&&!"".equals(beginDate)&&endDate!=null&&!"".equals(endDate)){
			if(beginDate.substring(0,7).equals(endDate.substring(0,7))){
				b=true;
			}
		}
		return b;
	}
	
	/**
	 * 转换日期为相应的字符串
	 * @return
	 */
	public static String getDateStr(Date date){
		return DateUtil.toString(date,FORMAT_YMD_1);
	}
	
	/**
	 * 转换日期为相应的字符串  //added by shine-canvas at 2014-06-27
	 * @return
	 */
	public static String getDateStrOfFORMAT_DATE_TIME_1(Date date){
		return DateUtil.toString(date,FORMAT_DATE_TIME_1);
	}
	
	/**
	 * 
	 * 上季度日期的获取
	 * 
	 */
	public static Date getSeason(){
		Calendar calender = Calendar.getInstance();
		calender.setTime(new Date());
		calender.add(Calendar.MONTH, -3);
		return calender.getTime();
		
	}
	
	/**
	 * 
	 * 上年度日期的获取
	 * 
	 */
	public static Date getYear(){
		Calendar calender = Calendar.getInstance();
		calender.setTime(new Date());
		calender.add(Calendar.YEAR, -1);
		return calender.getTime();
		
	}

	/**
	 * 判断字符是否为空
	 * @param str str
	 * @return  boolean
	 */
	public static boolean isEmpty(String str) {
		return ((str == null) || (str.length() == 0));
	}
	/**
	 * 获取离某日期时间前n天的日期时间
	 * @param nowTime 某日期时间
	 * @param days 天数
	 * @return
	 * @author wangxin
	 */
	public static String before(long nowTime,long days){
		long oldTime = nowTime - days*1000;
		//String res = new Timestamp(oldTime).toString();
		Date date = new Date(oldTime);
		return DateUtil.toString(date, DateUtil.FORMAT_DATE_TIME_1);		
	}
	/**
	 * 获取离某日期时间后n天的日期时间
	 * @param nowTime 某日期时间
	 * @param days 天数
	 * @return
	 * @author wangxin
	 */
	public static String after(long nowTime,long days){
		long oldTime = nowTime + days*1000;
		Date date = new Date(oldTime);
		return DateUtil.toString(date, DateUtil.FORMAT_DATE_TIME_1);		
	}
	
	/**
	 * 获取离某日期时间后n天的日期时间
	 * @param nowTime 某日期时间
	 * @param days 天数
	 * @return
	 * @author wangxin
	 */
	public static String testafter(long nowTime,long days){
		long oldTime = nowTime + 1000*10;
		Date date = new Date(oldTime);
		return DateUtil.toString(date, DateUtil.FORMAT_DATE_TIME_1);		
	}
	
	/**
	 * 将时间戳转换为"yyyy-MM-dd HH:mm:ss"格式的字符串
	 * @param timemille -- timemille 时间戳
	 * @return String -- "yyyy-mm-dd HH:MM:SS"格式的字符串
	 * @author wangxin
	 */
	public static String longToDateStr(long timemille)
	{
		SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdformat.format(timemille);
	}
	
	/**
	 * 将"yyyy-MM-dd HH:mm:ss"格式的字符串转换为时间戳,并且使时间戳的单位为秒
	 * @param date -- yyyy-mm-dd HH:MM:SS"格式的字符串
	 * @return String -- timemille 时间戳
	 * @author wangxin
	 */
	public static long dateToLong(String date)
	{
		SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			return sdformat.parse(date).getTime()/1000;//使时间戳的单位为秒，存入到数据库中
		} catch (ParseException e) {
			logger.error("时间格式String:" + date + " 转换为long失败" + e.getMessage());
			return 0l;
		}
	}
	
	/**
	 * 日期对象转换为时间戳
	 * @param date 日期对象
	 * @return String -- timemille 时间戳
	 * @author wangxin
	 */
	public static long dateObjectToLong(Date date)
	{
		SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return date.getTime()/1000;
	}
	
	/**
     * 将时间戳字符串转化为标准格式的日期时间字符串
     * 
     * @param timeStampStr 时间戳
     * @param format 转成成的时间格式字符串，参见DateUtil头部的各种格式定义字符串
     *  比如： 
	     public static final String FORMAT_DATE_TIME_8 = "MM月dd日";
     * @return 转换后的字符串
     * @author wangxin
     * @created at 2017年1月13日 
     */
    public static String changeTimeStampStrToDataTimeStr(String timeStampStr, String format)
    {
        String dateTimeStr = "";
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        try
        {
            if (StringUtils.isNotBlank(timeStampStr))
            {
                Long datetime = new Long(timeStampStr);
                dateTimeStr = simpleDateFormat.format(datetime);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return dateTimeStr;
    }
    
    /**
	 * 获取当前日期时间前n天或者后n天的日期时间
	 * @param daysOffset 某日期时间
	 * @return
	 * @author wangxin
	 */
	public static String getTimeStampStrOfSpecifiedDaysOffset(int daysOffset){
		//创建当前系统时间
		Calendar calendar = Calendar.getInstance();
		
		/**
		 * 查看daysOffset天以前日期时间或者daysOffset天以后的日期时间 
		 * 若想减去一个时间分量的值，
		 * 只需要传入一个相应的负数即可
		 * 
		 */
		calendar.add(Calendar.DAY_OF_YEAR, daysOffset);

		Date date = calendar.getTime();
		return String.valueOf(date.getTime()/1000);		
	}
	
	
	/**
	 * 获取当前系统的时间戳
	 * @return 格式为： 1397813103606
	 * @author wangxin
	 * //added by wangxin at 2017-03-8
	 */
	public static String getCurrentTimeStampStr(){
		//创建当前系统时间
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		return String.valueOf(date.getTime());		
	}
	
	/**
	 * 获取当前系统的时间戳
	 * @return 格式为： 1397813103606
	 * @author wangxin
	 */
	public static long getCurrentTimeStampLong(){
		//创建当前系统时间
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		return date.getTime();		
	}
	
	
	/**
     * 获取指定格式的日期时间字符串
     * @author wangxin
     * @param format 时间格式字符串
     * @return datetime 日期时间字符串
     */
    public static String getCurrentDataTimeStrWithFormat(String format)
    {
        String dateTimeStr = "";
        String timeStampStr = DateUtil.getCurrentTimeStampStr();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        try
        {
            if (StringUtils.isNotBlank(timeStampStr))
            {
                Long datetime = new Long(timeStampStr);
                dateTimeStr = simpleDateFormat.format(datetime);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return dateTimeStr;
    }
    
    /**
     * 获取当前日期后的n天的日期字符串
     * @author wangxin
     * @return datetime 日期字符串
     */
    public static String getDateStrAfterNDays(int days){
    	java.util.Calendar rightNow = java.util.Calendar.getInstance();
    	java.text.SimpleDateFormat sim = new java.text.SimpleDateFormat(FORMAT_YMD_1);	//得到当前时间，+3天
    	rightNow.add(java.util.Calendar.DAY_OF_MONTH, days); //如果是后退几天，就写 -天数 例如：	rightNow.add(java.util.Calendar.DAY_OF_MONTH, -3); //进行时间转换	
    	String date = sim.format(rightNow.getTime()); 
    	return date;
    }
    
//    public static void main(String[] args) {
//    	System.out.println("current Date:"+ DateUtil.getDateStr(new Date()));
//		System.out.println("30 days currentDate:"+ DateUtil.getDateStrAfterNDays(30));
//	}
    
    
    /**
     * 通过时间戳，获取指定日期对象
     * 
     * http://blog.csdn.net/thl331860203/article/details/5912435
     * 
     * @author wangxin
     * @version dayuanV1.0
     * @created at 2017年1月15日
     */
    public static Date getDateFromTimestampStr(String timeStampStr) {
		Date date = new Date(Long.parseLong(timeStampStr));
		//http://jingyan.baidu.com/article/fdbd4277c1dfccb89f3f4852.html?st=2&net_type=&bd_page_type=1&os=0&rst=&word=%E6%9C%AB%E5%BD%B1%E7%8F%8D%E7%8F%A0%E6%80%8E%E4%B9%88%E5%BE%97
		
		Calendar calender = Calendar.getInstance();
		calender.setTime(date);
		calender.set(Calendar.HOUR_OF_DAY, 0);
		calender.set(Calendar.MINUTE, 0);
		calender.set(Calendar.SECOND, 0);
		calender.set(Calendar.MILLISECOND, 0);

		return calender.getTime();
    }
    
    
    /**
     * 通过时间戳字符串，获取指定日期的零点的时间戳字符串
     * 
     * http://blog.csdn.net/thl331860203/article/details/5912435
     * 
     * @author wangxin
     * @version dayuanV1.0
     * @created at 2017年1月15日
     */
    public static String getStartTimeStampStrFromTimestampStr(String timeStampStr) {
		Date date = new Date(Long.parseLong(timeStampStr));
		//http://jingyan.baidu.com/article/fdbd4277c1dfccb89f3f4852.html?st=2&net_type=&bd_page_type=1&os=0&rst=&word=%E6%9C%AB%E5%BD%B1%E7%8F%8D%E7%8F%A0%E6%80%8E%E4%B9%88%E5%BE%97
		
		Calendar calender = Calendar.getInstance();
		calender.setTime(date);
		calender.set(Calendar.HOUR_OF_DAY, 0);
		calender.set(Calendar.MINUTE, 0);
		calender.set(Calendar.SECOND, 0);
		calender.set(Calendar.MILLISECOND, 0);

//		date = calender.getTime();
		String startTimeStampStr = String.valueOf(calender.getTimeInMillis());
		return startTimeStampStr;
    }
    
    
    /**
     * 通过时间戳常整型值，获取指定日期的零点的时间戳整型值
     * 
     * http://blog.csdn.net/thl331860203/article/details/5912435
     * 
     * @author wangxin
     * @version dayuanV1.0
     * @created at 2017年1月15日
     */
    public static long getStartTimeStampFromTimestamp(long timeStamp) {
		Date date = new Date(timeStamp);
		//http://jingyan.baidu.com/article/fdbd4277c1dfccb89f3f4852.html?st=2&net_type=&bd_page_type=1&os=0&rst=&word=%E6%9C%AB%E5%BD%B1%E7%8F%8D%E7%8F%A0%E6%80%8E%E4%B9%88%E5%BE%97
		
		Calendar calender = Calendar.getInstance();
		calender.setTime(date);
		calender.set(Calendar.HOUR_OF_DAY, 0);
		calender.set(Calendar.MINUTE, 0);
		calender.set(Calendar.SECOND, 0);
		calender.set(Calendar.MILLISECOND, 0);
		
//		TimeZone timeZone = TimeZone.getTimeZone("GMT+08:00");
//		calender.setTimeZone(timeZone);

//		date = calender.getTime();
		long startTimeStamp = calender.getTimeInMillis();
		return startTimeStamp;
    }
    
    
//    /**
//     * 通过时间戳常字符串值，获取指定日期的零点的时间戳字符串值
//     * 
//     * http://blog.csdn.net/thl331860203/article/details/5912435
//     * 
//     * @author wangxin
//     * @version dayuanV1.0
//     * @created at 2017年2月6日
//     */
//    public static String getStartTimeStampStrFromTimestampStr(String timeStampStr) {
//		long timeStamp = Long.parseLong(timeStampStr);
//    	
//    	Date date = new Date(timeStamp);
//		//http://jingyan.baidu.com/article/fdbd4277c1dfccb89f3f4852.html?st=2&net_type=&bd_page_type=1&os=0&rst=&word=%E6%9C%AB%E5%BD%B1%E7%8F%8D%E7%8F%A0%E6%80%8E%E4%B9%88%E5%BE%97
//		
//		Calendar calender = Calendar.getInstance();
//		calender.setTime(date);
//		calender.set(Calendar.HOUR_OF_DAY, 0);
//		calender.set(Calendar.MINUTE, 0);
//		calender.set(Calendar.SECOND, 0);
//		calender.set(Calendar.MILLISECOND, 0);
//
////		date = calender.getTime();
//		long startTimeStamp = calender.getTimeInMillis();
//		
//		String startTimeStampStr = String.valueOf(startTimeStamp);
//		
//		return startTimeStampStr;
//    }
    
    
    /**
     * 获取两个零点时间戳之间相隔的天数。开始和结束零点时间戳之间至少要相隔一天
     * 
     * @author wangxin
     * @version dayuanV1.0
     * @created at 2017年1月15日
     */
    public static int getDaysFromTwoStartTimeStamp(long startTimeStamp, long endTimeStamp) {
//    	if (endTimeStamp < startTimeStamp) {
//			return -1;
//		}
    	
    	int days= (int)((endTimeStamp - startTimeStamp) / (1000 * 60 * 60 * 24));
		return days;
    }
    
    
    /**
     * 获取起始和结束零点时间戳整型之间的包含的所有零点时间戳对象集合(不包含结束零点时间)
     * 
     * @author wangxin
     * @version dayuanV1.0
     * @created at 2017年1月15日
     */
    public static List<Long> getStartTimeStampListFromTwoStartTimeStamp(long startTimeStamp, long endTimeStamp) {
    	startTimeStamp = DateUtil.getStartTimeStampFromTimestamp(startTimeStamp);
    	endTimeStamp = DateUtil.getStartTimeStampFromTimestamp(endTimeStamp);
    	
    	List<Long> startTimeStampList = new ArrayList<Long>();
		
		int days = DateUtil.getDaysFromTwoStartTimeStamp(startTimeStamp, endTimeStamp);
		
		long tempStartTimeStamp = 0;
//		for (int i = 0; i <= days; i++) {
		for (int i = 0; i < days; i++) {//离店的日期，不需要报价
			tempStartTimeStamp = startTimeStamp + i * (1000 * 60 * 60 * 24);
			startTimeStampList.add(tempStartTimeStamp);
			System.out.println("tempStartTimeStamp:"+tempStartTimeStamp);
		}
		
//		new Set<Long>() {
//			public boolean add(Long e) {};
//		};
    	
		return startTimeStampList;
    }
    
    
    /**
     * 获取起始和结束零点时间戳整型之间的包含的所有零点时间戳对象集合。(包含结束零点时间)。专供运维设置数据使用
     * 输入参数为字符串格式的版本
     * 
     * @author wangxin
     * @version dayuanV1.0
     * @created at 2017年2月7日
     */
    public static List<Long> getStartTimeStampListFromTwoStartTimeStampStr(String startTimeStampStr, String endTimeStampStr) {
    	long startTimeStamp = Long.parseLong(startTimeStampStr);
    	long endTimeStamp = Long.parseLong(endTimeStampStr);
    	
    	startTimeStamp = DateUtil.getStartTimeStampFromTimestamp(startTimeStamp);
    	endTimeStamp = DateUtil.getStartTimeStampFromTimestamp(endTimeStamp);
    	
    	List<Long> startTimeStampList = new ArrayList<Long>();
		
		int days = DateUtil.getDaysFromTwoStartTimeStamp(startTimeStamp, endTimeStamp);
		
		
		long tempStartTimeStamp = 0;
//		for (int i = 0; i <= days; i++) {
		for (long i = 0; i <= days; i++) {//离店的日期，不需要报价
			tempStartTimeStamp = startTimeStamp + i * (1000 * 60 * 60 * 24);
			startTimeStampList.add(tempStartTimeStamp);
//			System.out.println("i:"+i+",increValue:"+i * (1000 * 60 * 60 * 24)+",tempStartTimeStamp:"+tempStartTimeStamp);
		}
		
//		new Set<Long>() {
//			public boolean add(Long e) {};
//		};
    	
		return startTimeStampList;
    }
    
//    /**
//     * 获取起始和结束零点时间戳整型之间的包含的所有零点时间戳对象集合
//     * 
//     * @author wangxin
//     * @version dayuanV1.0
//     * @created at 2017年1月15日
//     */
//    public static Set<Long> getStartTimeStampSetFromTwoStartTimeStamp(long startTimeStamp, long endTimeStamp) {
//    	startTimeStamp = DateUtil.getStartTimeStampFromTimestamp(startTimeStamp);
//    	endTimeStamp = DateUtil.getStartTimeStampFromTimestamp(endTimeStamp);
//    	
//    	Set<Long> startTimeStampSet = new HashSet<Long>();
//		
//		int days = DateUtil.getDaysFromTwoStartTimeStamp(startTimeStamp, endTimeStamp);
//		
//		long tempStartTimeStamp = startTimeStamp;
//		for (int i = 0; i < days; i++) {
//			tempStartTimeStamp = tempStartTimeStamp + (1000 * 60 * 60 * 24);
//			startTimeStampSet.add(tempStartTimeStamp);
//			System.out.println("tempStartTimeStamp:"+tempStartTimeStamp);
//		}
//		
////		new Set<Long>() {
////			public boolean add(Long e) {};
////		};
//    	
//		return startTimeStampSet;
//    }
    
    /**
     * 获取指定时间戳所在的日期的晚上8点的时间描述字符串。
     * 
     * @author wangxin
     * @version dayuanV1.0
     * @created at 2017年1月20日
     * @modified at 2017年1月23日
     */
    public static String getLatestArrivalTimeDescOfArrivalDate(long timeStamp) {
    	long startTimeStamp = DateUtil.getStartTimeStampFromTimestamp(timeStamp);
    			
		long latestArrivalTimeDescOfToday = startTimeStamp + 1 * (1000 * 60 * 60 * 20);
    	
		String latestArrivalTimeDescOfTodayDesc = DateUtil.changeTimeStampStrToDataTimeStr(String.valueOf(latestArrivalTimeDescOfToday), DateUtil.FORMAT_DATE_TIME_7);
		
		return latestArrivalTimeDescOfTodayDesc;
    }
    
    /**
     * 获取指定格式的时间字符串，所对应毫秒时间戳。
     * 
     * @author wangxin
     * @version dayuanV1.0
     * @created at 2017年2月08日
     */
    public static String getTimestampStrFromSpecificatedDateTimeStr(String dateTimeStr, String format) {
    	String timeStampStr = "0";
    	
    	SimpleDateFormat sdf = new SimpleDateFormat(format);
    	
    	Date date;
		try {
			date = sdf.parse(dateTimeStr);
		} catch (ParseException e) {
			e.printStackTrace();
			return timeStampStr;
		}
    	
    	long timeStampL = date.getTime();
    	
    	timeStampL = DateUtil.getStartTimeStampFromTimestamp(timeStampL);
    	
    	timeStampStr = String.valueOf(timeStampL);
    	
		return timeStampStr;
    }
    
    /**
     * 获取目标时间与当前时间两个零点时间戳之间相隔的分钟数。
     * 
     * @author wangxin
     * @version dayuanV1.0
     * @created by wangxin at 2017-03-8
     */
    public static int getMinutesFromTwoStartTimeStamp(long nowTimeStamp, long targetTimeStamp) {
//    	if (endTimeStamp < startTimeStamp) {
//			return -1;
//		}
    	
    	int minutes = (int)((targetTimeStamp - nowTimeStamp) / (1000 * 60));
		return minutes;
    }
    
    
    public static void main(String[] args) {
//		System.out.println(new Date().getTime());
//		System.out.println(DateUtil.getStartTimeStampStrFromTimestampStr(String.valueOf(new Date().getTime())));
//		1484409600000 01-15
//		1484841600000 01-20
		
//		long startDate = DateUtil.getStartTimeStampFromTimestamp(1484409600000L);
//		long endDate = DateUtil.getStartTimeStampFromTimestamp(1484841600000L);
		
//		DateUtil.getDaysFromTwoStartTimeStamp(startDate, endDate);
//		System.out.println("days:"+DateUtil.getDaysFromTwoStartTimeStamp(startDate, endDate));
		
		System.out.println(DateUtil.getStartTimeStampListFromTwoStartTimeStamp(1484409600023L, 1484841602000L));
	}
	/**
	 * 计算两个日期之间相差的天数
	 *
	 * @param smdate 较小的时间
	 * @param bdate  较大的时间
	 * @return 相差天数
	 * @throws ParseException
	 */
	public static int daysBetween(Date smdate, Date bdate) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		smdate = sdf.parse(sdf.format(smdate));
		bdate = sdf.parse(sdf.format(bdate));
		Calendar cal = Calendar.getInstance();
		cal.setTime(smdate);
		long time1 = cal.getTimeInMillis();
		cal.setTime(bdate);
		long time2 = cal.getTimeInMillis();
		long between_days = (time2 - time1) / (1000 * 3600 * 24);

		return Integer.parseInt(String.valueOf(between_days));
	}

	/**
	 * 得到几天前的时间
	 *
	 * @param d
	 * @param day
	 * @return
	 */
	public static Date getDateBefore(Date d, int day) {
		Calendar now = Calendar.getInstance();
		now.setTime(d);
		now.set(Calendar.DATE, now.get(Calendar.DATE) - day);
		return now.getTime();
	}

	/**
	 * 得到几天后的时间
	 *
	 * @param d
	 * @param day
	 * @return
	 */
	public static Date getDateAfter(Date d, int day) {
		Calendar now = Calendar.getInstance();
		now.setTime(d);
		now.set(Calendar.DATE, now.get(Calendar.DATE) + day);
		return now.getTime();
	}
}

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'Validation.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<script type="text/javascript" src="js/jquery-1.11.3.js"></script>
  </head>
  
  <body>
   
  <img id="img" src="Validation/image" onclick="refresh()">  
  <input type="text" name="verification" onblur="checkIsExist();" id="verification">
   <br><span  id="showResult"></span> 
<script type="text/javascript">

		function refresh() {  
		    var url = "Validation/image?number="+Math.random();
		    //本地缓存了结果，不加随机数浏览器就从缓存里找，不会请求后台，加随机数则浏览器认为它是一个新的请求，就会执行后台接口 
		    $("#img").attr("src",url);  
		} 
		
		function checkIsExist() {  
		    var verification = $.trim($("#verification").val());  
		    $.ajax({  
		        type:"POST",   //http请求方式  
		        url:"Validation/checkCode?verification="+verification, //发送给服务器的url  
		        dataType:"json",  //告诉JQUERY返回的数据格式(注意此处数据格式一定要与提交的controller返回的数据格式一致,不然不会调用回调函数complete)  
		        complete:function(msg) {  
		        	var json = JSON.parse(msg.responseText)
		            if (json.code==1) {  
		                $("#showResult").html("<font color='green'>验证码正确</font>");  
		            } else {  
		                $("#showResult").html("<font color='red'>验证码错误</font>");  ;  
		            }  
		        }//定义交互完成,并且服务器正确返回数据时调用回调函数   
		    });  
		}  
		   
	</script>
  </body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'Test.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
   <form action="UEditor/tj" method="post" enctype="multipart/form-data">
   <textarea id="editor_id" name="content" style="width:100%;height:300px;">  
      这里输入内容...  
</textarea>  
 <button type="submit" class="but1">提交</button>
  </body>
  
<script charset="utf-8" src="kindeditor/kindeditor.js"></script>  
<script charset="utf-8" src="kindeditor/lang/zh_CN.js"></script>  
<script>  
KindEditor.ready(function(K) {  
    K.create('#editor_id', {  
      uploadJson : 'kindeditor/jsp/upload_json.jsp',  
      fileManagerJson : 'kindeditor/jsp/file_manager_json.jsp',  
      allowFileManager : true  
    });  
});    
</script>  

</html>
